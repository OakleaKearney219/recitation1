package recitation1;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.geometry.*;
import javafx.stage.Stage;

/**
 *
 * @author Oaklea
 */
public class Recitation1 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        Button btn2 = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        btn2.setText("Say 'Goodbye Cruel World!");
        btn2.setOnAction(new EventHandler<ActionEvent>(){
           @Override
           public void handle(ActionEvent event2){
               System.out.println("Goodbye Cruel World!");
           }
        });
        
        GridPane root = new GridPane();
        root.setAlignment(Pos.CENTER);
        root.setVgap(10);
        
        root.getChildren().add(btn);
        root.setRowIndex(btn,1);
        root.getChildren().add(btn2);
        root.setRowIndex(btn2,2);
        
        Scene scene = new Scene(root, 300, 250);
        
        primaryStage.setTitle("Hello, Goodbye");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
